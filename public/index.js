/**
 * Created by drjr on 17-1-22.
 */
//多次抽取，一次抽取10个，注意抽中的人不能再次参与抽奖
//第一次调用抽取函数时，应该初始化一个数组，之后不用再调用初始化函数
//所以上一次抽奖完毕的数组应该作为保留，并且已经剔除掉抽中的人

//初始化一个1_100的数组
var data = require('../data');

function initArray(data) {
    var newArray = [];
    for (let i = 0; i < data.length; i++) {
        newArray[i] = data[i].name;
    }

    return newArray;
}

//抽取规定人数的中奖者
function extractionNumber(n) {
    var nextArray;
    var award = [];
    //一次抽取一个，共抽取10次
    for (var i = 0; i < n; i++) {
        var index = Math.floor(Math.random() * array.length);
        award.push(...array.splice(index, 1) //剔除已经抽中的元素
    )
        ;
    }
    // nextArray = array;
    return award;
}

var array = initArray(data);


//随机洗牌
//total 抽奖总人数
//n 规定中奖人数
function shuffle() {
    array.sort(() => 0.5 - Math.random());
    return array;
}

shuffle();
console.log(extractionNumber(10));

//可以考虑把初始化数组写成一个构造器